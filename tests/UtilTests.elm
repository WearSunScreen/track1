module UtilTests exposing (suite)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, constant, int, intRange, list, string)
import List exposing (length, member, product, sum)
import Random exposing (Seed, initialSeed)
import Test exposing (..)
import Util exposing ((?:), dropWhile, elementAt, randomSelect, replaceAt, rotate)


suite : Test
suite =
    let
        primes =
            [ 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 ]

        fibs =
            [ 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 ]

        longList =
            List.range -200000 200000

        seed1 =
            initialSeed 1

        seed2 =
            initialSeed 2
    in
        describe "Test Util functions"
            [ describe "Testing Elvis operator"
                [ test "(?:) fails on Just" <|
                    \_ ->
                        List.head ([ 1, 2, 3 ])
                            ?: 0
                            |> Expect.equal 1
                , test "(?:) fails on Nothing" <|
                    \_ ->
                        List.head ([])
                            ?: 0
                            |> Expect.equal 0
                ]
            , describe "testing Util.rotate"
                [ test "fails on rotate 1" <|
                    \_ ->
                        rotate 1 [ 1, 2, 3, 4 ]
                            |> Expect.equal [ 2, 3, 4, 1 ]
                , test "fails on rotate 2" <|
                    \_ ->
                        rotate 2 [ "a", "b", "c" ]
                            |> Expect.equal [ "c", "a", "b" ]
                , test "fails on rotate -1" <|
                    \_ ->
                        rotate -1 [ 1, 2, 3 ]
                            |> Expect.equal [ 3, 1, 2 ]
                , test "fails on rotate 0" <|
                    \_ ->
                        rotate 0 [ 1, 2, 3 ]
                            |> Expect.equal [ 1, 2, 3 ]
                , test "fails on rotate 7" <|
                    \_ ->
                        rotate 7 [ 1, 2, 3, 4, 5 ]
                            |> Expect.equal [ 3, 4, 5, 1, 2 ]
                , test "fails on rotate on single element list" <|
                    \_ ->
                        rotate 3 [ 1 ]
                            |> Expect.equal [ 1 ]
                , test "fails on rotate on empty list" <|
                    \_ ->
                        rotate 1 []
                            |> Expect.equal []
                ]
            , describe "testing Util.replaceAt"
                [ test "fails on replaceAt 1" <|
                    \_ ->
                        replaceAt 1 11 [ 1, 2, 3, 4 ]
                            |> Expect.equal [ 11, 2, 3, 4 ]
                , test "fails on replaceAt 2" <|
                    \_ ->
                        replaceAt 2 "bb" [ "a", "b", "c" ]
                            |> Expect.equal [ "a", "bb", "c" ]
                , test "fails on replaceAt with empty list" <|
                    \_ ->
                        replaceAt 2 "bb" []
                            |> Expect.equal [ "bb" ]
                , test "fails on replaceAt with too large index" <|
                    \_ ->
                        replaceAt 12 "bb" [ "a", "b", "c" ]
                            |> Expect.equal [ "a", "b", "c", "bb" ]
                , test "fails on replaceAt with negative index" <|
                    \_ ->
                        replaceAt -1 "bb" [ "a", "b", "c" ]
                            |> Expect.equal [ "bb", "a", "b", "c" ]
                , test "fails on replaceAt with 0 index" <|
                    \_ ->
                        replaceAt 0 "bb" [ "a", "b", "c" ]
                            |> Expect.equal [ "bb", "a", "b", "c" ]
                ]
            , describe "Testing Util.elementAt"
                [ test "fails on elementAt with negative index" <|
                    \_ ->
                        elementAt -1 [ 1, 2, 3 ]
                            |> Expect.equal Nothing
                , test "fails on elementAt with zero index" <|
                    \_ ->
                        elementAt 0 [ 1, 2, 3 ]
                            |> Expect.equal Nothing
                , test "fails on elementAt with out of range index" <|
                    \_ ->
                        elementAt 10 [ 1, 2, 3 ]
                            |> Expect.equal Nothing
                , test "fails on elementAt with empty list" <|
                    \_ ->
                        elementAt 1 []
                            |> Expect.equal Nothing
                , test "fails on elementAt with valid index" <|
                    \_ ->
                        elementAt 2 [ 1, 2, 3 ]
                            |> Expect.equal (Just 2)
                ]
            , describe "Testing Util.randomSelect"
                [ test "fails when randomSelect takes an element of the list" <|
                    \_ ->
                        Expect.equal
                            (Tuple.first (randomSelect seed1 [ 1, 1, 1 ]))
                            (Just 1)
                , test "fails on randomSelect from a list of primes" <|
                    \_ ->
                        Expect.true "randomSelect a prime number"
                            (List.member
                                ((Tuple.first (randomSelect seed1 primes)) ?: -12)
                                primes
                            )
                , test "fails on randomSelect from a list of fibonnacci numbers" <|
                    \_ ->
                        Expect.true "randomSelect a fibonacci number"
                            (List.member
                                ((Tuple.first (randomSelect seed1 fibs)) ?: -12)
                                fibs
                            )
                , test "randomSelect should varies when given different seed " <|
                    \_ ->
                        Expect.notEqual
                            (Tuple.first (randomSelect seed1 longList))
                            (Tuple.first (randomSelect seed2 longList))
                , test "randomSelect should return a new seed" <|
                    \_ ->
                        Expect.notEqual
                            (Tuple.second (randomSelect seed1 primes))
                            seed1
                , test "randomSelect fails with empty list" <|
                    \_ ->
                        (Tuple.first (randomSelect seed1 []))
                            |> Expect.equal Nothing
                ]
            , describe "Testing Util.moveTheseToFront"
                [ fuzz2
                    (list int)
                    (list int)
                    "order may change, sum should not change"
                    (\items xs ->
                        Util.moveTheseToFront items xs
                            |> sum
                            |> Expect.equal (sum xs)
                    )
                , fuzz2
                    (list (intRange -1 4))
                    (list (intRange 0 9))
                    "when order is changed, sum should remain the same"
                    (\items xs ->
                        Util.moveTheseToFront items xs
                            |> sum
                            |> Expect.equal (sum xs)
                    )
                , fuzz2
                    (list (intRange -1 4))
                    (list (intRange 0 9))
                    "length should be unaffected"
                    (\items xs ->
                        Util.moveTheseToFront items xs
                            |> length
                            |> Expect.equal (length xs)
                    )
                , fuzz2
                    (constant [ 0 ])
                    (list (intRange -1 10))
                    "all zeros in the list should be moved to front"
                    (\items xs ->
                        Util.moveTheseToFront items xs
                            |> dropWhile ((==) 0)
                            |> member 0
                            |> Expect.false "but zeros were not moved to front"
                    )
                ]
            ]
