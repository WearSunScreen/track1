module Track1Tests exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import List exposing (filter, length)
import Maybe exposing (..)
import Model exposing (..)
import Random exposing (initialSeed)
import Test exposing (..)
import Tuple exposing (first, second)


suite : Test
suite =
    let
        testString =
            "aBCDeFGHiJ"

        mInit =
            Model.init |> first

        model =
            { mInit
                | challenge =
                    ( testString, (mInit.challenge |> second) )
            }

        mLong =
            { model
                | challenge =
                    ( testString
                    , [ AllCaps
                      , DropFirst
                      , JustVowels
                      , Reverse
                      ]
                    )
            }

        mLong2 =
            { model
                | challenge =
                    ( testString
                    , [ AllLower
                      , DropLast
                      , JustConsonants
                      , Rotate
                      ]
                    )
            }

        ( challengeLong, _ ) =
            generateChallenge mLong.randomSeed mLong.numberOfEffects allEffectsNames
    in
        describe "Track 1 Challenge App"
            [ describe "Testing Effect functions"
                [ test "piping several effects should produce expected string" <|
                    \_ ->
                        testString
                            |> Model.allLower
                            |> Model.dropLast
                            |> Model.justConsonants
                            |> Model.rotate
                            |> Expect.equal "cdfghb"
                , test "piping allCaps and other effects should produce expected string" <|
                    \_ ->
                        testString
                            |> Model.allCaps
                            |> Model.dropFirst
                            |> Model.justVowels
                            |> Model.reverse
                            |> Expect.equal "IE"
                ]
            , describe "Test that we can find the solution of any challenge"
                [ test "solving a long challenge should produce expected result" <|
                    \_ ->
                        mLong2.challenge
                            |> Model.currentSolution
                            |> Expect.equal "cdfghb"
                , test "solving a very long challenge should produce expected result" <|
                    \_ ->
                        mLong.challenge
                            |> Model.currentSolution
                            |> Expect.equal "IE"
                , test "the initial model should not be a winner" <|
                    \_ ->
                        Model.isWinner mInit |> Expect.equal False
                , test "a model with winning effects should be a winner" <|
                    \_ ->
                        let
                            mWinner =
                                { mLong
                                    | effectsSelected = mLong.challenge |> second
                                }
                        in
                            Model.isWinner mWinner |> Expect.equal True
                , test "Test isWinner on long challenge" <|
                    \_ ->
                        let
                            mWinner =
                                { mLong2
                                    | effectsSelected = mLong2.challenge |> second
                                }
                        in
                            Model.isWinner mWinner |> Expect.equal True
                , test "Test isWinner on another long challenge" <|
                    \_ ->
                        let
                            mWinner =
                                { mInit
                                    | effectsSelected = mInit.challenge |> second
                                }
                        in
                            Model.isWinner mWinner |> Expect.equal True
                ]
            , describe "Test isValidChallenge"
                [ fuzz int "generateChallenge should only give valid challenges" <|
                    \seed ->
                        Model.generateChallenge (initialSeed seed) 9 Model.allEffectsNames
                            |> first
                            |> Model.isValidChallenge
                            |> Expect.true "only valid challenges"
                , fuzz int "challenges with 9 effects shouldn't create 0 length solutions" <|
                    \seed ->
                        Model.generateChallenge (initialSeed seed) 9 Model.allEffectsNames
                            |> first
                            |> second
                            |> filter (\e -> not (List.member e Model.allEffectsNames))
                            |> length
                            |> Expect.equal 0
                , fuzz int "challenges with 9 effects shouldn't create zero length solution" <|
                    \seed ->
                        Model.generateChallenge (initialSeed seed) 9 Model.allEffectsNames
                            |> first
                            |> Model.currentSolution
                            |> String.length
                            |> Expect.greaterThan 0
                , fuzz int "challenges with 4 effects shouldn't create zero length solution" <|
                    \seed ->
                        Model.generateChallenge (initialSeed seed) 4 Model.allEffectsNames
                            |> first
                            |> Model.currentSolution
                            |> String.length
                            |> Expect.greaterThan 0
                , test "challenges should never have reverse twice" <|
                    \_ ->
                        ( testString, [ Reverse, Reverse ] )
                            |> Model.isValidChallenge
                            |> Expect.false "but has two reverses"
                , test "challenges should never filter to vowels by twice" <|
                    \_ ->
                        ( testString, [ JustVowels, JustVowels ] )
                            |> Model.isValidChallenge
                            |> Expect.false "but calls JustVowels twice"
                , test "challenges should never filter to consonants twice" <|
                    \_ ->
                        ( testString, [ JustConsonants, JustConsonants ] )
                            |> Model.isValidChallenge
                            |> Expect.false "but calls JustConsonants twice"
                , test "challenges should never strip vowels and consonants" <|
                    \_ ->
                        ( testString, [ JustVowels, JustConsonants ] )
                            |> Model.isValidChallenge
                            |> Expect.false "but strips all characters"
                , test "challenges should never strip all upper and lower case letters" <|
                    \_ ->
                        ( testString, [ AllLower, AllCaps ] )
                            |> Model.isValidChallenge
                            |> Expect.false "but calls AllLower and AllCaps"
                , test "never call justVowels and allLower (presumes string only had lower case vowels" <|
                    \_ ->
                        ( testString, [ JustVowels, AllLower ] )
                            |> Model.isValidChallenge
                            |> Expect.false "calls both justVowels and allLower"
                ]
            ]
