module OneOfTests exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import OneOf exposing (..)
import Random exposing (Seed)
import Test exposing (..)


suite : Test
suite =
    describe "Not really a test"
        [ describe "Just a way, compare fonts"
            [ test "to see ligatures used in Elm" <|
                \_ ->
                    Expect.true "all should be true"
                        (List.all
                            ((==) True)
                            [ 1 >= 1
                            , 1 <= 1
                            , 1 /= 0
                            , 1 :: [ 2, 3 ] == [ 1, 2, 3 ]
                            ]
                        )
            , test "compostion ligatures" <|
                \_ ->
                    let
                        g =
                            (\n -> n * 2) << (\n -> n * 3)

                        h =
                            g >> (\n -> n * 4)
                    in
                        Expect.equal (h 2) (2 * 2 * 3 * 4)
            , test "simliar looking character are distinct" <|
                \_ ->
                    let
                        x =
                            "1LilIlse10O"

                        y =
                            "1lilo0"

                        z =
                            "1LILO0"
                    in
                        Expect.notEqual x y
            ]
        ]
