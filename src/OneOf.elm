module OneOf exposing (..)

{-| This custom Random.Generator returns a one element list with
    a randomly selected element of the provided list.
    If given an empty list it returns an empty list
-}

import List exposing (drop, length, take)
import Random exposing (Generator, Seed, int, map, step)


{-| Test app
import Html exposing (text)
import List exposing (drop, length, take)
import Random exposing (Generator, Seed, initialSeed, int, map, step)

main =
  text
    <| toString
    <| Tuple.first
    <| manyOf (initialSeed 12) 20 (List.range 1 99) []
-}
manyOf : Seed -> Int -> List a -> List a -> ( List a, Seed )
manyOf seed n source acc =
    let
        ( x, seed2 ) =
            step (select source) seed
    in
        if n < 1 then
            ( acc, seed )
        else
            ( x
                ++ (Tuple.first
                        (manyOf seed2 (n - 1) source acc)
                   )
            , seed2
            )


oneOf : Seed -> List a -> List a -> ( List a, Seed )
oneOf seed source acc =
    step (select source) seed


select : List a -> Generator (List a)
select list =
    map (\y -> list |> drop y |> take 1) (int 0 ((length list) - 1))
