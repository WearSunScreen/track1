module Util exposing (..)

{-| A place to keep some handy functions that may be used in many files
    Ajustments to solarized-dark-color-theme.json
    	"editor.lineHighlightBackground": "#382152",
		"editor.selectionBackground": "#7E0E4F",
-}

import List exposing (drop, length, member, range, take)
import Maybe exposing (Maybe, withDefault)
import Random exposing (Seed, int, step)


{-| Range operator
-}
(<->) : Int -> Int -> List Int
(<->) a b =
    range a b


{-| The "Elvis" operator abbreviates calling Maybe.withDefault
-}
(?:) : Maybe a -> a -> a
(?:) m d =
    withDefault d m


{-| The "unhappy Elvis" operator, is used when Nothing is unexpected or an error
-}
(?:/) : Maybe a -> a -> a
(?:/) m d =
    case m of
        Just x ->
            x

        Nothing ->
            Debug.log "WARNING! Expected value: " d


{-| Drop elements in order as long as the predicate evaluates to `True`
-}
dropWhile : (a -> Bool) -> List a -> List a
dropWhile predicate list =
    case list of
        [] ->
            []

        x :: xs ->
            if (predicate x) then
                dropWhile predicate xs
            else
                list


{-| return the nth item of a list (one-relative index)
-}
elementAt : Int -> List a -> Maybe a
elementAt n list =
    if n < 1 then
        Nothing
    else
        case List.drop (n - 1) list of
            [] ->
                Nothing

            y :: ys ->
                Just y


{-| Move specified items to the front of a list
-}
moveTheseToFront : List a -> List a -> List a
moveTheseToFront items list =
    moveToFront (\x -> member x items) list


{-| Move specified items to the front of a list
-}
moveToFront : (a -> Bool) -> List a -> List a
moveToFront pred list =
    case list of
        [] ->
            []

        x :: xs ->
            if pred x then
                x :: moveToFront pred xs
            else
                moveToFront pred xs ++ [ x ]


{-| Randomly select one item from a list
-}
randomSelect : Seed -> List a -> ( Maybe a, Random.Seed )
randomSelect seed list =
    let
        ( idx, newSeed ) =
            step (int 1 (length list)) seed
    in
        ( elementAt idx list, newSeed )


{-| replace a single element in a list,
    Index is 1 relative,
    negative index value prepends to list
    index larger than list length appends to list
-}
replaceAt : Int -> a -> List a -> List a
replaceAt idx value list =
    List.take (idx - 1) list ++ value :: List.drop (idx) list


{-| rotate the elements of a list, negative count rotates in the opposite direction
-}
rotate : Int -> List a -> List a
rotate n list =
    case list of
        [] ->
            []

        _ ->
            let
                r =
                    n % (List.length list)
            in
                List.drop r list ++ List.take r list


{-| Take elements in order as long as the predicate evaluates to `True`
-}
takeWhile : (a -> Bool) -> List a -> List a
takeWhile predicate xs =
    case xs of
        [] ->
            []

        hd :: tl ->
            if (predicate hd) then
                hd :: takeWhile predicate tl
            else
                []
