module Update exposing (..)

import List exposing (length, repeat)
import Model exposing (..)
import Random exposing (Seed, int, initialSeed, maxInt, minInt, step)
import Task exposing (Task, perform)
import Time exposing (now)
import Tuple exposing (first, second)
import Util exposing ((?:/), replaceAt, rotate)


checkWinner : Model -> Model
checkWinner m =
    if m |> isWinner then
        { m
            | isPaused = True
            , numberCorrect = m.numberCorrect + 1
        }
    else
        m


subs : Model -> Sub Msg
subs model =
    Sub.none


update : Msg -> Model -> ( Model, Cmd Msg )
update msg m =
    let
        newChallenge : Int -> ( Challenge, Seed )
        newChallenge n =
            generateChallenge m.randomSeed n allEffectsNames
    in
        case msg of
            EffectPicked idx pickedEffect ->
                ({ m
                    | effectsSelected =
                        replaceAt idx
                            (getEffectOfDisplayName pickedEffect)
                            m.effectsSelected
                 }
                    |> checkWinner
                )
                    ! []

            FirstChallenge ->
                let
                    ( newC, newS ) =
                        newChallenge 2
                in
                    { m
                        | challenge = newC
                        , effectsSelected = repeat 2 NoChange
                        , isPaused = False
                        , numberCorrect = 0
                        , numberOfEffects = 2
                        , randomSeed = newS
                    }
                        ! [ Task.perform StartApp Time.now ]

            IncreaseChallenge ->
                let
                    ( newC, newS ) =
                        newChallenge (m.numberOfEffects + 1)
                in
                    { m
                        | challenge = newC
                        , effectsSelected = repeat (m.numberOfEffects + 1) NoChange
                        , isPaused = False
                        , numberCorrect = 0
                        , numberOfEffects = m.numberOfEffects + 1
                        , randomSeed = newS
                    }
                        ! [ Cmd.none ]

            NextChallenge ->
                let
                    ( newC, newS ) =
                        newChallenge m.numberOfEffects
                in
                    { m
                        | challenge = newC
                        , effectsSelected = repeat m.numberOfEffects NoChange
                        , isPaused = False
                        , randomSeed = newS
                    }
                        ! [ Cmd.none ]

            RestartChallenge ->
                let
                    ( newC, newS ) =
                        newChallenge 2
                in
                    { m
                        | challenge = newC
                        , effectsSelected = repeat 2 NoChange
                        , isPaused = False
                        , numberCorrect = 0
                        , numberOfEffects = 2
                        , randomSeed = newS
                    }
                        ! [ Cmd.none ]

            SkipChallenge ->
                let
                    ( newC, newS ) =
                        newChallenge m.numberOfEffects
                in
                    { m
                        | challenge = newC
                        , effectsSelected = repeat m.numberOfEffects NoChange
                        , isPaused = False
                        , randomSeed = newS
                    }
                        ! [ Cmd.none ]

            StartApp time ->
                ( { m
                    | startTime = Just time
                    , randomSeed = initialSeed (truncate time * 1000)
                  }
                , Cmd.none
                )
