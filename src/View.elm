module View exposing (view)

import Html exposing (Html, button, div, h1, h2, img, option, p, select, text)
import Html.Attributes exposing (attribute, height, src, selected, style, value, width)
import Html.Events exposing (onClick, onInput)
import List exposing (append, length, map, repeat, take)
import Model exposing (..)
import Tuple exposing (first, second)


{-| Values used throughout this module
-}
k =
    { borderWidth = 1
    , cellWidth = 140
    , imgHeight = 110
    , maxEffects = 4
    , effectsY = 340
    }


{-| Values that will change for localization
-}
l =
    { btnGiveAnother = "Give another!"
    , btnIncrease = "Bring it on!"
    , harder = "Do you want to level up?!"
    , letsStart = "Let's start!"
    , replay = "Play again!"
    , selectTransforms = "Select the correct Effects Units to change"
    , skip = "skip this one"
    , soFar = " right so far!"
    , welcome = "Welcome the Track 1 Challenge!"
    , winnerAll = "Woot! You smashed the Challenge!"
    , winnerLevel = "Extra! You conquered the level!"
    }


box : Html Msg -> Int -> Html Msg
box e n =
    div
        [ style (boxStyle n)
        ]
        [ h2 [] [ e ] ]


boxImg : String -> Int -> Html Msg
boxImg url n =
    div
        [ style (( "height", px k.imgHeight ) :: List.filter (\( x, y ) -> x /= "height") (boxStyle n))
        ]
        [ box (img [ src url ] []) 1 ]


{-| draw a div with s text, n units wide, center justification
-}
boxTC : Int -> String -> Html Msg
boxTC n s =
    boxT "center" n s


{-| draw a div with s text, w units wide, j justification
-}
boxT : String -> Int -> String -> Html Msg
boxT j w s =
    div
        [ style <| ( "text-align", j ) :: (boxStyle w)
        ]
        [ h2 [] [ s |> text ] ]


boxStyle : Int -> List ( String, String )
boxStyle w =
    [ ( "background-color", "papayawhip" )
    , ( "display", "block" )
    , ( "float", "left" )
    , ( "padding", "0px" )
    , ( "width", px (w * (k.cellWidth)) )
    ]


dialogStyle : List ( String, String )
dialogStyle =
    [ ( "position", "absolute" )
    , ( "font-size", "100%" )
    , ( "top", "30px" )
    , ( "left", "200px" )
    , ( "background-color", "rosybrown" )
    ]


effectImg : Int -> String -> Html Msg
effectImg idx url =
    div
        [ style (effectXY idx -10 -5) ]
        [ (img [ src url, height 60, width 200 ] []) ]


effectXY : Int -> Int -> Int -> List ( String, String )
effectXY idx xOffset yOffset =
    xyPairs ((100 + xOffset) + (220 * idx)) (k.effectsY + yOffset)


mainStyle : Int -> List ( String, String )
mainStyle w =
    [ ( "padding", "10px" )
    , ( "width", px (w * k.cellWidth) )
    ]


monitorImg : Int -> String -> Html Msg
monitorImg idx url =
    div
        [ style (terminalXY idx -110 -20) ]
        [ (img [ src url, height 90, width 220 ] []) ]


px : Int -> String
px n =
    (toString n) ++ "px"


view : Model -> Html Msg
view m =
    case m.startTime of
        Nothing ->
            viewWelcome m

        Just t ->
            viewStuff m


viewCables : Model -> List (Html Msg)
viewCables m =
    let
        cableImg : Int -> Int -> Int -> String -> Int -> Int -> Html Msg
        cableImg idx xOffset yOffset url h w =
            div
                [ style (effectXY idx xOffset yOffset) ]
                [ (img [ src url, height h, width w ] []) ]
    in
        cableImg 0 -35 -42 "img/cableTextToEffect.png" 110 135
            :: cableImg ((length m.effectsSelected) - 0) -80 -35 "img/cableToMonitor.png" 110 140
            :: List.map
                (\idx -> (cableImg idx 135 25 "img/cableEffectToEffect.png" 60 115))
                (List.range 0 ((length m.effectsSelected) - 2))


viewDialog : Model -> List (Html Msg)
viewDialog m =
    let
        hasWon =
            m.numberCorrect >= m.numberToWin
    in
        if m.isPaused then
            [ div [ dialogStyle |> style ]
                [ p []
                    [ if hasWon then
                        if m.numberOfEffects < m.maxEffects then
                            h1 []
                                [ p [] [ text l.winnerLevel ]
                                , p [] [ text l.harder ]
                                , button [ onClick IncreaseChallenge ] [ text l.btnIncrease ]
                                , button [ onClick FirstChallenge ] [ text l.replay ]
                                ]
                        else
                            h1 []
                                [ p [] [ text l.winnerAll ]
                                , button [ onClick FirstChallenge ] [ text l.replay ]
                                ]
                      else
                        h1 []
                            [ p [] [ text ((toString m.numberCorrect) ++ l.soFar) ]
                            , button [ onClick NextChallenge ] [ text l.btnGiveAnother ]
                            ]
                    ]
                ]
            ]
        else
            []


viewSelect : Int -> (String -> msg) -> List EffectName -> EffectName -> Html msg
viewSelect idx msgConstructor allFx selectedOption =
    let
        optionize s x =
            option
                [ value (getDisplayNameOfEffect x), selected (s == x) ]
                [ text (getDisplayNameOfEffect x) ]
    in
        select
            [ onInput msgConstructor
            , style (effectXY idx 30 0)
            ]
            (List.map (optionize selectedOption) allFx)


viewScore : Int -> Int -> Int -> List (Html Msg)
viewScore w soFar toWin =
    [ div [ mainStyle w |> style ]
        ((repeat soFar (boxImg "img/winSmall.png" 1))
            ++ (repeat (toWin - soFar) (boxImg "img/loseSmall.png" 1))
            ++ (repeat (w - toWin) (boxTC 1 "."))
        )
    ]


{-| this needs to be easier to read
-}
viewStuff : Model -> Html Msg
viewStuff m =
    let
        challengeText =
            (m.challenge |> first) ++ " - ----> to <---- - " ++ (currentSolution m.challenge)

        maxWidth =
            m.maxEffects + 2

        textWidth =
            (maxWidth - 1) // 2
    in
        div [ mainStyle maxWidth |> style ]
            (viewScore maxWidth m.numberCorrect m.numberToWin
                ++ [ h1 [ style (xyPairs 170 180) ] [ text challengeText ]
                   , button [ onClick SkipChallenge, style (xyPairs 40 410) ] [ text l.skip ]
                   , terminalImg 0 "img/hello-world.png"
                   , monitorImg (m.numberOfEffects) "img/monitor.png"
                   , h2 [ style (terminalXY (m.numberOfEffects) -64 0) ] [ text (Model.userSolution m) ]
                   ]
                ++ List.map
                    (\idx -> (effectImg idx "img/effect-unit.png"))
                    (List.range 0 ((length m.effectsSelected) - 1))
                ++ List.indexedMap
                    (\idx e ->
                        (viewSelect
                            idx
                            (EffectPicked (idx + 1))
                            Model.effectNames
                            e
                        )
                    )
                    m.effectsSelected
                ++ viewCables m
                ++ viewDialog m
            )


viewWelcome : Model -> Html Msg
viewWelcome m =
    div [ mainStyle 4 |> style ]
        [ p [] [ text "---" ]
        , p [] [ text "add hint to start, flash over select of effect" ]
        , p [] [ text "center monitor text, fit text" ]
        , p [] [ text "colorize challenge text" ]
        , p [] [ text "video capture play" ]
        , p [] [ text "localize to spanish" ]
        , p [] [ h1 [] [ text l.welcome ] ]
        , button [ onClick FirstChallenge ] [ text l.letsStart ]
        ]


terminalImg : Int -> String -> Html Msg
terminalImg idx url =
    div
        [ style (terminalXY idx 0 0) ]
        [ (img [ src url, height 60, width 200 ] []) ]


terminalXY : Int -> Int -> Int -> List ( String, String )
terminalXY idx xOffset yOffset =
    xyPairs ((25 + xOffset) + (220 * idx)) (k.effectsY - 80 + yOffset)


xyPairs : Int -> Int -> List ( String, String )
xyPairs x y =
    [ ( "left", px x )
    , ( "position", "absolute" )
    , ( "top", px y )
    ]
