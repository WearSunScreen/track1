module Model exposing (..)

import Dict exposing (Dict, get)
import Html exposing (Html, text)
import List exposing (filter, head, length, map, member)
import Maybe exposing (Maybe, withDefault)
import OneOf exposing (manyOf)
import Random exposing (Generator, Seed, int)
import String exposing (contains, dropLeft, dropRight, fromChar, left)
import Time exposing (Time)
import Tuple exposing (first, second)
import Util exposing ((?:), (?:/), elementAt, moveTheseToFront)


-- types


type alias Challenge =
    ( String, List EffectName )


type EffectName
    = AllCaps
    | AllLower
    | DropFirst
    | DropLast
    | JustConsonants
    | JustVowels
    | NoChange
    | Reverse
    | Rotate


type alias Effect =
    ( EffectName, String -> String, String )


type alias Model =
    { challenge : Challenge
    , effectsSelected : List EffectName
    , isPaused : Bool
    , maxEffects : Int
    , numberCorrect : Int
    , numberOfEffects : Int
    , numberToWin : Int
    , randomSeed : Seed
    , startTime : Maybe Time
    }


type Msg
    = EffectPicked Int String
    | FirstChallenge
    | IncreaseChallenge
    | NextChallenge
    | RestartChallenge
    | SkipChallenge
    | StartApp Time



-- Values that will change for localization


l =
    { hello = "Hello World!"
    , allCaps = "AllCaps"
    , allLower = "AllLower"
    , dropFirst = "DropFirst"
    , dropLast = "DropLast"
    , justConsonants = "JustConsonants"
    , justVowels = "JustVowels"
    , noChange = "NoChange"
    , reverse = "Reverse"
    , rotate = "Rotate"
    }


{-| List of "effects" available to the app user
-}
allEffects : List Effect
allEffects =
    [ ( AllCaps, allCaps, l.allCaps )
    , ( AllLower, allLower, l.allLower )
    , ( DropFirst, dropFirst, l.dropFirst )
    , ( DropLast, dropLast, l.dropLast )
    , ( JustConsonants, justConsonants, l.justConsonants )
    , ( JustVowels, justVowels, l.justVowels )
    , ( NoChange, identity, l.noChange )
    , ( Reverse, reverse, l.reverse )
    , ( Rotate, rotate, l.rotate )
    ]


{-| List of "effects" available to the app user
-}
allEffectsNames : List EffectName
allEffectsNames =
    List.map (\( x, _, _ ) -> x) allEffects
        |> filter (\x -> x /= NoChange)


challenges =
    [ ( l.hello, [ "rotate", "dropLast" ] )
    , ( l.hello, [ "allCaps", "reverse" ] )
    , ( l.hello, [ "allLower", "justConsonants" ] )
    , ( l.hello, [ "justConsonants", "dropLast" ] )
    , ( l.hello, [ "rotate", "rotate" ] )
    , ( l.hello, [ "justVowels", "reverse" ] )
    ]


{-| Give the solution of the current challenge
-}
currentSolution : Challenge -> String
currentSolution c =
    let
        names =
            c |> second

        fs =
            List.map (\s -> getFunctionOfEffect s) names

        applyF : (String -> String) -> String -> String
        applyF f s =
            f s
    in
        List.foldl applyF (first c) fs


{-| Localizable list of effect names
-}
effectNames : List EffectName
effectNames =
    [ AllCaps
    , AllLower
    , DropFirst
    , DropLast
    , JustConsonants
    , JustVowels
    , NoChange
    , Reverse
    , Rotate
    ]


getDisplayNameOfEffect : EffectName -> String
getDisplayNameOfEffect e =
    map (\( n, f, d ) -> ( n, d )) allEffects
        |> filter (\( nn, dd ) -> nn == e)
        |> head
        |> withDefault ( NoChange, "inconceivable!" )
        |> second


getEffectOfDisplayName : String -> EffectName
getEffectOfDisplayName s =
    map (\( n, f, d ) -> ( d, n )) allEffects
        |> Dict.fromList
        |> get s
        |> withDefault NoChange


getFunctionOfEffect : EffectName -> (String -> String)
getFunctionOfEffect e =
    map (\( n, f, d ) -> ( n, f )) allEffects
        |> filter (\( nn, ff ) -> nn == e)
        |> head
        |> withDefault ( NoChange, identity )
        |> second


generateChallenge : Seed -> Int -> List EffectName -> ( Challenge, Seed )
generateChallenge seed n effects =
    let
        ( fx, seed2 ) =
            manyOf seed n effects []

        -- justVowels is moved to front so it doesn't nullify effects such a dropFirst, dropLasg
        fxSorted =
            fx
                |> moveTheseToFront [ JustVowels, JustConsonants ]
    in
        if isValidChallenge ( l.hello, fxSorted ) then
            ( ( l.hello, fxSorted )
            , seed2
            )
        else
            -- Debug.log "generated invalid challenge" (generateChallenge seed2 n effects)
            generateChallenge seed2 n effects


getStartingValue : Model -> String
getStartingValue model =
    model.challenge
        |> Tuple.first


init : ( Model, Cmd Msg )
init =
    { challenge = ( l.hello, [ Rotate, DropLast ] )
    , effectsSelected = [ NoChange, NoChange ]
    , isPaused = False
    , maxEffects = 4
    , numberCorrect = 0
    , numberOfEffects = 2
    , numberToWin = 3
    , randomSeed = Random.initialSeed 0
    , startTime = Nothing
    }
        ! []


isValidChallenge : Challenge -> Bool
isValidChallenge ( s, es ) =
    if 1 < (es |> (filter (\x -> member x [ AllCaps, AllLower ])) |> length) then
        -- calling allCaps or allLower multiple times is redundant or create empty solution
        False
    else if 1 < (es |> (filter (\x -> member x [ JustConsonants, JustVowels ])) |> length) then
        -- calling allCaps or allLower multiple times is redundant or create empty solution
        False
    else if member AllLower es && member JustVowels es then
        -- calling allLower and justVowels is redundant (presumes all vowels are lowercase)
        False
    else if 1 < (es |> (filter (\x -> member x [ Reverse ])) |> length) then
        False
    else if 1 > (currentSolution ( s, es ) |> String.length) then
        -- never allow a zero length solution to a challenge
        False
    else
        True


isWinner : Model -> Bool
isWinner m =
    (userSolution m) == (currentSolution m.challenge)


{-| Give the solution of the user's selections
-}
userSolution : Model -> String
userSolution m =
    let
        userEffects =
            List.map
                (\x -> getFunctionOfEffect x)
                m.effectsSelected

        applyF : (String -> String) -> String -> String
        applyF f s =
            f s
    in
        List.foldl applyF (getStartingValue m) userEffects



-- Functions implementing the user selected transformation effects


allCaps : String -> String
allCaps s =
    String.toUpper s


allLower : String -> String
allLower s =
    String.toLower s


dropFirst : String -> String
dropFirst s =
    dropLeft 1 s


dropLast : String -> String
dropLast s =
    dropRight 1 s


justConsonants : String -> String
justConsonants s =
    String.filter (\c -> contains (fromChar c) "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ") s


justVowels : String -> String
justVowels s =
    String.filter (\c -> contains (fromChar c) "aeiouAEIOU") s


monitor : String -> Html Msg
monitor v =
    Html.text v


noChange =
    identity


reverse : String -> String
reverse s =
    String.reverse s


rotate : String -> String
rotate s =
    (dropLeft 1 s) ++ (left 1 s)
