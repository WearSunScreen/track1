module Main exposing (..)

{-|
    This will be used in an online editor along with the Track 1 Elm code companion.
    We need this import line to load some basic support to output html text
-}

import Html exposing (..)


{-|
    "main" is elm's way of saying, this is where app starts
-}
main =
    -- the next line is our app,
    helloWorld |> allCaps |> reverse |> allLower |> monitor


{-|
    Ignore everything below. Nothing magic here, just some simple
    Elm code to create our track 1 devices. A few tracks into the
    Guitarist's Guide and all this stuff will be easy to understand
-}
monitor : String -> Html String
monitor v =
    Html.text v


allCaps : String -> String
allCaps s =
    String.toUpper s


allLower : String -> String
allLower s =
    String.toLower s


reverse : String -> String
reverse s =
    String.reverse s


helloWorld : String
helloWorld =
    "Hello World"


foobar : String
foobar =
    "Foobar"
